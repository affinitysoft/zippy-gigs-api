import os
import paypalrestsdk
import braintree
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

DB_CONFIG = dict(database="zippy_db", user="zippy_gig",
                 password="123", host="localhost", port=5432,
                 register_hstore=False, autorollback=True)

SECRET_KEY = 'top-secret'
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')
MEDIA_URL = '/media'

"""
    Braitree credentials
    Sandbox for testing.

    Sandbox credentials
    email : oldtigersvoice@gmail.com
    password : iphone3gs
"""
# braintree.Configuration.configure(braintree.Environment.Sandbox,
#                                   merchant_id="hcs8xgtp45f7qn8k",
#                                   public_key="zkjskdmjsdbbp9ds",
#                                   private_key="d8eff4a65785fd72ff3d449e528da5a4")
braintree.Configuration.configure(braintree.Environment.Production,
                                  merchant_id="7vs8jkrs3sjdw4yt",
                                  public_key="7kdbfjfktq8b7p7z",
                                  private_key="dc7023236c23ccd117575dc6a34ff684")


PAYMENT_USERID = 'vlad.skliar-facilitator_api1.gmail.com'
PAYMENT_PASSWORD = 'ZHPYJ7QSV2LB43HW'
PAYMENT_SIGNATURE = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AaFZj-wLqNpOvI94N54KB2jEGKIP'
PAYMENT_APPLICATION_ID = 'APP-80W284485P519543T'  # sandbox app id
PAYMENT_SANDBOX = True

PAYMENT_HEADERS = {
    'X-PAYPAL-SECURITY-USERID': PAYMENT_USERID,
    'X-PAYPAL-SECURITY-PASSWORD': PAYMENT_PASSWORD,
    'X-PAYPAL-SECURITY-SIGNATURE': PAYMENT_SIGNATURE,
    # this application-id is the sandbox value, same for everyone
    'X-PAYPAL-APPLICATION-ID': PAYMENT_APPLICATION_ID,
    'X-PAYPAL-REQUEST-DATA-FORMAT': 'JSON',
    'X-PAYPAL-RESPONSE-DATA-FORMAT': 'JSON'
}

ENDPOINT_PAY = 'https://svcs.paypal.com/AdaptivePayments/'
PAYMENT_URL = 'https://www.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey='
if PAYMENT_SANDBOX:
    ENDPOINT_PAY = 'https://svcs.sandbox.paypal.com/AdaptivePayments/'
    PAYMENT_URL = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey='

paypalrestsdk.configure({
  'mode': 'sandbox',
  'client_id': 'AQkquBDf1zctJOWGKWUEtKXm6qVhueUEMvXO_-MCI4DQQ4-LWvkDLIN2fGsd',
  'client_secret': 'EL1tVxAjhT7cJimnz5-Nsx9k2reTKSVfErNQF-CmrwJgxRtylkGTKlU4RvrX'
})

# email server
MAIL_SERVER = 'secure.emailsrvr.com'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME = 'noreply@zippygigs.com'
MAIL_PASSWORD = '2Beornot2be!'
MAIL_DEFAULT_SENDER = 'noreply@zippygigs.com'

# administrator list
ADMINS = ['oldtigersvoice@gmail.com']
