from hashlib import sha1
from itsdangerous import JSONWebSignatureSerializer
from flask import g, url_for
from zippy_gig.base import BaseController, ApiException
import phonenumbers
from zippy_gig.models import Account
from zippy_gig.constants import AccountType
from zippy_gig.utils import hash_pswd
import config


class ResetWithTokenController(BaseController):

    def __init__(self, request):
        super(ResetWithTokenController, self).__init__(request)

    def _call(self, token): 
        try:
            s = JSONWebSignatureSerializer(config.SECRET_KEY)
            email = s.loads(token, salt="recover-key")
        except Exception as e:
            raise ApiException('Token is invalid')

        if self.request.method == 'POST':
            account = self._check_email(email)
            account.password = hash_pswd(self._verify_field("password"))
            account.save()

        return 'Password reset'

    def _check_email(self, email):
        try:
            account = Account.get(Account.email == email)
            return account
        except Account.DoesNotExist:
            raise ApiException("Account {email} does not exists".format(email=email))