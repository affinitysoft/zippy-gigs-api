from hashlib import sha1

from flask import g
from zippy_gig.base import BaseController, ApiException
import phonenumbers
from zippy_gig.constants import AccountType


class EditAvailablilityDateController(BaseController):

	def __init__(self, request):
		super(EditAvailablilityDateController, self).__init__(request)

	def _call(self):
		availability_from = self.parse_time(self._verify_field('availability_from'))
		availability_to = self.parse_time(self._verify_field('availability_to'))
		g.account.availability = "{time_from}-{time_to}".format(time_from=availability_from, time_to=availability_to)
		g.account.save()

		return g.account.get_profile()

	def parse_time(self, time_str):
		time_list = time_str.split("-")
		if len(time_list) != 2:
			time_list = time_str.split(":")
			if len(time_list) != 2:
				raise ApiException("Invalid availability time: %s" % time_str)
		else:
			hours = time_list[0]
			minutes = time_list[1]
			return "{hours}:{minutes}".format(hours=hours, minutes=minutes)
