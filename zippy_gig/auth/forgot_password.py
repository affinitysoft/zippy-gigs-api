from itsdangerous import JSONWebSignatureSerializer
from flask import g, url_for, render_template
from zippy_gig.base import BaseController, ApiException
import phonenumbers
from zippy_gig.constants import AccountType
from zippy_gig.models import Account
import config
from flask_mail import Message
from zippy_gig import mail


class ForgotPasswordController(BaseController):

    def __init__(self, request):
        super(ForgotPasswordController, self).__init__(request)

    def _call(self):
        account = self._check_email()

        s = JSONWebSignatureSerializer(config.SECRET_KEY)

        token = s.dumps(account.email, salt='recover-key')

        recover_url = 'http://zippygigs.com/reset/{}'.format(token)

        html = render_template(
            'email/recover.html',
            recover_url=recover_url)

        subject = "Password reset requested"

        self._send_email([account.email], subject, html)

        return "Reset url was sent to your email"

    def _check_email(self):
        email = self._verify_field("email")
        try:
            account = Account.get(Account.email == email)
            return account
        except Account.DoesNotExist:
            raise ApiException("Account {email} does not exists".format(email=email))

    def _send_email(self, recipients, subject, html_body):
        msg = Message(subject, recipients=recipients)
        msg.html = html_body
        mail.send(msg)



