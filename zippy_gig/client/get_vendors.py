from zippy_gig.base import BaseController, ApiException
from zippy_gig.models import Account
from flask import g


class GetVendorsController(BaseController):

    def __init__(self, request):
        super(GetVendorsController, self).__init__(request)

    def _call(self):
        job_type = self._verify_param('job_type')
        vendor_status = self._verify_param('status')
        radius = self._verify_param('radius')
        hourly_rate_gt = self._verify_param('hourly_rate_gt')
        hourly_rate_lt = self._verify_param('hourly_rate_lt')

        return [account.get_data() for account in Account.get_vendors(job_type=job_type,
                                                                      vendor_status=vendor_status,
                                                                      account_id=g.account.id,
                                                                      account_type=g.account.type,
                                                                      radius=radius,
                                                                      hourly_rate_gt=hourly_rate_gt,
                                                                      hourly_rate_lt=hourly_rate_lt)]

    def _verify_param(self, name):
        return self.request.args.get(name, None)
