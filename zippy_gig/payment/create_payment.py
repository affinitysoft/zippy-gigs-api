import requests
import json
from flask import g
from zippy_gig.base import BaseController, ApiException
from zippy_gig.models import Payment, Order
from zippy_gig.utils import decimal_default


class GetPaymentUrlController(BaseController):

    def __init__(self, request):
        super(GetPaymentUrlController, self).__init__(request)

    def _call(self):
        order_id = self._verify_field("order_id")
        account = g.account
        return self._find_payment(account, self._check_order(order_id))

    def _check_order(self, order_id):
        try:
            order = Order.get(Order.id == order_id)
            return order
        except Order.DoesNotExist:
            raise ApiException("Order (id=%s) doesn't exist" % order_id)

    def _find_payment(self, account, order):
        from config import PAYMENT_URL
        try:
            payment = Payment.get(Payment.account == account, Payment.order == order)
            PAYMENT_URL += payment.payment_id
            return PAYMENT_URL
        except:
            return self._create_payment(account, order)

    def _create_payment(self, account, order):
        from config import PAYMENT_HEADERS, ENDPOINT_PAY, PAYMENT_URL
        payload = {
            'actionType': 'PAY',
            'currencyCode': 'USD',
            'receiverList': {
                'receiver': [
                    {'amount': order.money_to_pay, 'email': order.vendor.pay_pal}
                ]
            },
            'returnUrl': 'http://www.example.com/success.html', 
            'cancelUrl': 'http://www.example.com/cancel.html',
            'requestEnvelope': {
                'errorLanguage': 'en_US',
                'detailLevel': 'ReturnAll'
            },
            'memo': 'Payment description here',
        }
        ENDPOINT_PAY += 'Pay'
        r = requests.post(ENDPOINT_PAY, data=json.dumps(payload, default=decimal_default), headers=PAYMENT_HEADERS, )
        if r.status_code == 200:
            if r.json()['responseEnvelope']['ack'] == 'Success':
                payment = Payment.create(account=account,
                                         order=order,
                                         payment_id=r.json()['payKey']
                                         )
                PAYMENT_URL += payment.payment_id
                return PAYMENT_URL
            else:
                raise ApiException("Payment error: %s" % r.json()[0]['error']['message'])
        else:
            raise ApiException("Payment creation problem occurred. Please try again or contact the support")

