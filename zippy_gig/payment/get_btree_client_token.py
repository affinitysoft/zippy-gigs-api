from zippy_gig.base import BaseController
import braintree


class GetBtreeClientToken(BaseController):
    def _call(self):
        return braintree.ClientToken.generate()
