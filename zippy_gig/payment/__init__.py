import braintree
from flask import Blueprint, request, jsonify
from zippy_gig.decorators import jsonify_result
from zippy_gig import token_auth
from flask import render_template
from zippy_gig.payment.create_payment import GetPaymentUrlController
from zippy_gig.payment.get_btree_client_token import GetBtreeClientToken
from zippy_gig.payment.create_subscription import CreateSubscription


payment = Blueprint('payment', __name__, url_prefix='/api/v1/payment')


@payment.route("/", methods=["POST"])
@token_auth.login_required
@jsonify_result
def get_payment_url():
    return GetPaymentUrlController(request)()


@payment.route("/client_token/", methods=["GET"])
@token_auth.login_required
@jsonify_result
def client_token():
    return GetBtreeClientToken(request)()


@payment.route("/create-subscription/", methods=["POST"])
@token_auth.login_required
@jsonify_result
def create_subscription():
    return CreateSubscription(request)()
