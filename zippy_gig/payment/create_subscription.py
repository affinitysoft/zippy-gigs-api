import jsonpickle
import json
from flask import g
import braintree
from zippy_gig.base import BaseController


plan_mapper = {
	1: 'dp96',
	2: 'tmng',
	3: '2ymb'
}

class CreateSubscription(BaseController):
	def _call(self):
		if g.account.customer_id:
			customer = braintree.Customer.find(g.account.customer_id)
			if customer:
				result = braintree.Subscription.create({
					"payment_method_token": customer.payment_methods[0].token,
					"plan_id": plan_mapper[int(self._verify_field("plan"))]
				})
				g.account.vendor_membership = True
				g.account.save()
				return json.loads(jsonpickle.encode(result))

		result = braintree.Customer.create({
			"first_name": g.account.first_name,
			"last_name": g.account.last_name,
			"company": "TestCompany",
			"email": g.account.email,
			"phone": g.account.phone,
			"fax": "614.555.5678",
			"website": "www.example.com",
			"payment_method_nonce": self._verify_field("nonce")
		})
		if result.is_success:
			g.account.customer_id = result.customer.id
			result = braintree.Subscription.create({
				"payment_method_token": result.customer.payment_methods[0].token,
				"plan_id": plan_mapper[int(self._verify_field("plan"))]
			})
			g.account.vendor_membership = True
			g.account.save()
			return json.loads(jsonpickle.encode(result))
		return None
