from zippy_gig.base import BaseController, ApiException
from zippy_gig.models import db, AccountJobType, JobType, Account
from flask import g


class AddAccountJobTypeController(BaseController):

    def __init__(self, request):
        super(AddAccountJobTypeController, self).__init__(request)

    def _call(self):
        account = g.account
        job_type_id = self._verify_field("job_type_id")
        hourly_rate = self.request.get_json().get('hourly_rate', None)
        return self._create_account_job_type(account, job_type_id, hourly_rate).to_dict()

    def _create_account_job_type(self, account, job_type_id, hourly_rate=None):
        with db.transaction():
            account_job_type = AccountJobType.get_or_create(account=account,
                                                            job_type=self._check_job_type(job_type_id))
            if account_job_type[1] and hourly_rate:
                account_job_type[0].hourly_rate = hourly_rate
                account_job_type[0].save()
            else:
                raise ApiException("JobType (id=%s) for this account already exists" % job_type_id)
        return account_job_type[0]

    def _check_job_type(self, job_type_id):
        try:
            job_type = JobType.get(JobType.id == job_type_id)
            return job_type
        except JobType.DoesNotExist:
            raise ApiException("JobType (id=%s) doesn't exist" % job_type_id)
