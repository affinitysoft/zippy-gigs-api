from zippy_gig.base import BaseController, ApiException
from zippy_gig.models import db, AccountJobType, JobType, Account
from flask import g


class RemoveAccountJobTypeController(BaseController):

    def __init__(self, request):
        super(RemoveAccountJobTypeController, self).__init__(request)

    def _call(self):
        account = g.account
        job_type_id = self._verify_field("job_type_id")
        return self._remove_account_job_type(account, job_type_id)

    def _remove_account_job_type(self, account, job_type_id):
        with db.transaction():
            try:
                account_job_type = AccountJobType.get(account=account,
                                                      job_type=self._check_job_type(job_type_id))
                account_job_type.delete_instance()
                return {'message': "Job type (id=%s) for this account is deleted" % job_type_id}
            except AccountJobType.DoesNotExist:
                raise ApiException("Job type (id=%s) for this account doesn't exist" % job_type_id)

    def _check_job_type(self, job_type_id):
        try:
            job_type = JobType.get(JobType.id == job_type_id)
            return job_type
        except JobType.DoesNotExist:
            raise ApiException("JobType (id=%s) doesn't exist" % job_type_id)
