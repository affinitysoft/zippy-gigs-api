from flask import Blueprint, request
from zippy_gig.vendor.set_vendor_status import SetVendorStatusController
from zippy_gig.vendor.get_vendor_status import GetVendorStatusController
from zippy_gig.vendor.set_vendor_description import SetVendorDescriptionController
from zippy_gig.vendor.get_vendor_description import GetVendorDescriptionController
from zippy_gig.vendor.add_account_job_type import AddAccountJobTypeController
from zippy_gig.vendor.remove_account_job_type import RemoveAccountJobTypeController
from zippy_gig.vendor.get_gigs import GetVendorGigsController
from zippy_gig.decorators import jsonify_result, validate_json
from zippy_gig import token_auth

vendor = Blueprint('vendor', __name__, url_prefix='/api/v1/vendor')


@vendor.route("/status/", methods=["POST"])
@token_auth.login_required
@validate_json
@jsonify_result
def set_vendor_status():
    return SetVendorStatusController(request)()


@vendor.route("/status/", methods=["GET"])
@token_auth.login_required
@validate_json
@jsonify_result
def get_vendor_status():
    return GetVendorStatusController(request)()


@vendor.route("/description/", methods=["POST"])
@token_auth.login_required
@validate_json
@jsonify_result
def set_vendor_description():
    return SetVendorDescriptionController(request)()


@vendor.route("/description/", methods=["GET"])
@token_auth.login_required
@validate_json
@jsonify_result
def get_vendor_description():
    return GetVendorDescriptionController(request)()


@vendor.route("/add-job-type/", methods=['POST'])
@token_auth.login_required
@jsonify_result
def add_job_type():
    return AddAccountJobTypeController(request)()


@vendor.route("/remove-job-type/", methods=['POST'])
@token_auth.login_required
@jsonify_result
def remove_job_type():
    return RemoveAccountJobTypeController(request)()


@vendor.route("/gigs/", methods=['GET'])
@token_auth.login_required
@jsonify_result
def get_gigs():
    return GetVendorGigsController(request)()
