from flask import g
from zippy_gig.base import BaseController


class GetVendorGigsController(BaseController):

    def __init__(self, request):
        super(GetVendorGigsController, self).__init__(request)

    def _call(self):
        """
            Check get parameters and get query
        """
        price_gt = self._verify_param('price_gt')
        price_lt = self._verify_param('price_lt')
        gig_type = self._verify_param('gig_type')
        radius = self._verify_param('radius')
        query = g.account.get_gigs(price_gt=price_gt,
                                   price_lt=price_lt,
                                   gig_type=gig_type,
                                   radius=radius)
        return [_gig.get_gig() for _gig in query]

    def _verify_param(self, name):
        return self.request.args.get(name, None)
