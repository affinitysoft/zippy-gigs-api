from enum import Enum


class AccountType(Enum):
    Client = 1
    Vendor = 2
    ClientAndVendor = 3


class AccountStatus(Enum):
    In = 1
    Out = 2


class OrderStatus(Enum):
    Pending = 1
    Accepted = 2
    Rejected = 3
    Finished = 4

PAGINATION_ORDER_PER_PAGE = 2

