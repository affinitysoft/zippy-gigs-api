from zippy_gig.base import BaseController, ApiException
from zippy_gig.models import Gig, Account
from zippy_gig.constants import AccountType
from zippy_gig.utils import get_dictionary_from_model
from flask import g


class CreateWorkController(BaseController):

    def __init__(self, request):
        super(CreateWorkController, self).__init__(request)

    def _call(self):
        vendor_id = self.request.get_json().get('vendor_id', None)
        work = Gig(_type=self._verify_field('type'),
                   description=self._verify_field('description'),
                   price=self._verify_field('price'),
                   account=g.account,
                   vendor=self._check_vendor(vendor_id))

        work.save()
        return work.get_gig()

    def _check_vendor(self, vendor_id=None):
        if vendor_id:
            try:
                vendor = Account.get(Account.id == vendor_id)
                if vendor.type in (AccountType.Vendor.value, AccountType.ClientAndVendor.value):
                    return vendor
                else:
                    raise ApiException("Account (id=%s) is not a vendor" % vendor_id)
            except Account.DoesNotExist:
                raise ApiException("Account (id=%s) doesn't exist" % vendor_id)
        else:
            return None
