import logging
from flask import Flask
from flask_admin import Admin
from flask_cors import CORS
from flask_httpauth import HTTPTokenAuth
from flask_mail import Mail
from zippy_gig.utils import JSONEncoder
from httpauth import HTTPBasicAuth
from zippy_gig.admin import AccountAdmin
from zippy_gig.models import Account

app = Flask(__name__, static_url_path='/static', static_folder='static', template_folder='admin/templates')
app.config.from_object('config')
mail = Mail(app)
app.debug = True
app.json_encoder = JSONEncoder
admin = Admin(app, name='Zippy Gigs Admin', template_mode='bootstrap3')
admin.add_view(AccountAdmin(Account))

# CORS(app)
# cors = CORS(app, resources={r"/": {"origins": "*"}})
# app.config['CORS_HEADERS'] = 'Content-Type'
# logging.getLogger('flask_cors').level = logging.DEBUG

# CORS(app)
# app.session_interface = RedisSessionInterface()
token_auth = HTTPBasicAuth(scheme="Token")
# token_auth = HTTPTokenAuth(scheme='Token')

from zippy_gig.auth import auth
from zippy_gig.client import client
from zippy_gig.error import error
from zippy_gig.gig import gig
from zippy_gig.vendor import vendor
from zippy_gig.order import order
from zippy_gig.payment import payment

app.register_blueprint(auth)
app.register_blueprint(client)
app.register_blueprint(error)
app.register_blueprint(gig)
app.register_blueprint(vendor)
app.register_blueprint(order)
app.register_blueprint(payment)
