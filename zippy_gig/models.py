# -*- coding: utf8 -*-
import os
import random
from hashlib import sha1
from werkzeug.utils import secure_filename

from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
import peeweedbevolve
from peewee import (Model, CharField, TextField, ForeignKeyField, IntegerField, SmallIntegerField,
                    DateTimeField, DoubleField, BooleanField, DecimalField, datetime as peewee_datetime,
                    fn)

from playhouse.pool import PooledPostgresqlExtDatabase
from playhouse.signals import post_save
from flask import Markup, request, g


from config import DB_CONFIG, SECRET_KEY, MEDIA_ROOT, MEDIA_URL
from utils import get_dictionary_from_model
from zippy_gig.constants import AccountType, OrderStatus

from geopy.geocoders import Nominatim

db = PooledPostgresqlExtDatabase(**DB_CONFIG)
db.commit_select = True
db.autorollback = True

import zippy_gig.sql_additional
import decimal

peewee_now = peewee_datetime.datetime.now


class _Model(Model):

    class Meta:
        database = db


class Photo(_Model):

    class Meta:
        db_table = "photos"

    image = CharField(null=True)

    def __unicode__(self):
        return self.image

    def save_image(self, file_obj):
        self.image = secure_filename(file_obj.filename)
        full_path = os.path.join(MEDIA_ROOT, self.image)
        file_obj.save(full_path)
        self.save()

    def url(self):
        return os.path.join(MEDIA_URL, self.image)

    def thumb(self):
        return Markup('<img src="%s" style="height: 80px;" />' % self.url())


# to set zip_code use only set_zip_code method!
class Account(_Model):
    """
        Provider/Client account model
    """
    class Meta:
        db_table = "accounts"

    email = CharField(unique=True, max_length=320)
    password = CharField()
    is_active = BooleanField(default=True)
    created = DateTimeField(default=peewee_now)
    first_name = CharField(null=True, max_length=255)
    last_name = CharField(null=True, max_length=255)
    address = TextField(null=True)
    phone = CharField(null=True)
    alt_phone = CharField(null=True)  # alternative phone
    pay_pal = CharField(null=True)
    avatar = ForeignKeyField(Photo, null=True)
    type = SmallIntegerField(null=True, default=3)  # account type: 1 - client | 2 - vendor | 3 - both
    zip_code = CharField(null=True)
    lng = DecimalField(null=True)
    lat = DecimalField(null=True)
    availability = CharField(null=True)  # in format: 10:00 - 18:00
    skype = CharField(null=True)
    first_login = BooleanField(default=True)

    # provider's specific fields
    vendor_status = SmallIntegerField(null=True, default=2)  # 1 - in | 2 - out
    vendor_description = TextField(null=True)
    vendor_membership = BooleanField(default=False)
    response_rating = DecimalField(null=True,
                                   max_digits=10,
                                   decimal_places=2,
                                   verbose_name="Response rating")
    satisfaction_rating = DecimalField(null=True,
                                       max_digits=10,
                                       decimal_places=2,
                                       verbose_name="Satisfaction rating")
    customer_id = CharField(null=True) # uses as braintree customer id
    subscription_expired = DateTimeField(null=True)

    def __repr__(self):
        return "{class_name}(id={id})".format(class_name=self.__class__.__name__, id=self.id)

    def to_dict(self):
        return get_dictionary_from_model(self)

    def get_data(self):
        data = get_dictionary_from_model(self)
        data.pop("password")
        data.update({"job_types": self.get_account_job_types()})
        data.update({"avatar_url": self.avatar.url() if self.avatar else None})
        return data

    def get_profile(self, pk=None):
        account = None
        if pk:
            account = Account.get(id=pk)
            if account:
                data = account._data.items()
            else:
                return None
        else:
            data = self._data.items()
        profile_data = ["id", "first_name", "last_name", "address", "phone", "alt_phone", "pay_pal", "type",
                        "vendor_description", "zip_code", "availability", "vendor_status", "response_rating",
                        "satisfaction_rating", "skype", "vendor_membership", "first_login"]
        data = {key: item for key, item in data if key in profile_data}
        if account:
            data.update({"avatar_url": account.avatar.url() if account.avatar else None})
        else:
            data.update({"avatar_url": self.avatar.url() if self.avatar else None})
        data.update({"job_types": self.get_account_job_types(pk)})
        return data

    @staticmethod
    def get_vendors(job_type=None, vendor_status=None, account_id=None, account_type=None,
                    radius=None, hourly_rate_gt=None, hourly_rate_lt=None):
        condition = ((Account.type == AccountType.Vendor.value)\
                     | (Account.type == AccountType.ClientAndVendor.value))

        if account_type in {AccountType.Vendor.value, AccountType.ClientAndVendor.value}:
            condition &= (Account.id != account_id)

        if radius and g.account.lat and g.account.lng:
            condition &= (fn.distance_kilometers(g.account.lat, g.account.lng, Account.lat, Account.lng) <= radius)

        condition &= (Account.first_login == False)

        _ret_val = Account.select().where(condition)

        # TODO: change querying. .join() ?
        if job_type:
            condition = (AccountJobType.job_type == job_type)
            if hourly_rate_gt:
                condition &= (AccountJobType.hourly_rate >= hourly_rate_gt)
            if hourly_rate_lt:
                condition &= (AccountJobType.hourly_rate <= hourly_rate_lt)
            account_ids = [item.account.id for item in AccountJobType.select().where(condition)]
            if account_ids:
                _ret_val = _ret_val.select()\
                    .where(Account.id << account_ids)
            else:
                _ret_val = _ret_val.select()\
                    .where(Account.id << [None])

        if vendor_status:
            _ret_val = _ret_val.select().where(Account.vendor_status == vendor_status)

        return _ret_val

    def get_gigs(self, price_gt=None, price_lt=None, gig_type=None, radius=None):
        if self.type in (AccountType.Vendor.value, AccountType.ClientAndVendor.value):
            condition = Gig.is_active
            condition &= ((Gig.vendor == self.id) | (Gig.vendor == None))
            condition &= (Gig.account != self.id)
            if price_gt:
                condition &= (Gig.price > price_gt)
            if price_lt:
                condition &= (Gig.price < price_lt)
            if gig_type:
                condition &= (Gig._type == gig_type)
            if radius and g.account.lat and g.account.lng:
                condition &= (fn.distance_kilometers(g.account.lat, g.account.lng, Gig.account.lat, Gig.account.lng) <= radius)
            query = Gig.select().where(condition)
            return query
        else:
            return {}

    def generate_auth_token(self, expiration=600):
        s = Serializer(SECRET_KEY, expires_in=expiration)
        return s.dumps({'id': self.id})

    def get_job_types(self):
        query = JobType.select().join(AccountJobType).join(Account).where(Account.id == self.id)
        return [{'id': job.id, 'title': job.title} for job in query]

    def get_account_job_types(self, pk=None):
        _id = pk or self.id
        query = AccountJobType.select().join(Account).where(Account.id == _id)
        return [{'id': job.job_type.id, 'title': job.job_type.title, 'hourly_rate': job.hourly_rate} for job in query]

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(SECRET_KEY)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        account = Account.get(Account.id == data['id'])
        return account

    def upload_photo(self):
        if 'avatar' in request.files:
            _file = request.files['avatar']
            photo = Photo.create(image=_file.filename)
            photo.save_image(_file)
            self.avatar = photo
            print("self.avatar: ", self.avatar)
            self.save()
            return os.path.join(MEDIA_URL, photo.image)
        return None

    def set_zip_code(self, _zip_code):
        self.zip_code = _zip_code

        geo_locator = Nominatim()
        location = geo_locator.geocode(self.zip_code)
        if location:
            self.lat = location.latitude
            self.lng = location.longitude
        else:
            self.lat = None
            self.lng = None

        del geo_locator

    def payment_data(self):
        data = {}
        data['first_name'] = self.first_name
        data['last_name'] = self.last_name
        data['email'] = self.email
        return data

    @staticmethod
    def calculate_rating(order):
        try:
            orders = Order.select().where((Order.vendor == order.vendor) & (Order.status == OrderStatus.Finished.value))
            response_rating = decimal.Decimal('0.00')
            satisfaction_rating = decimal.Decimal('0.00')
            for order in orders:
                response_rating += order.response_rating
                satisfaction_rating += order.satisfaction_rating
            order.vendor.response_rating = response_rating / len(orders)
            order.vendor.satisfaction_rating = satisfaction_rating / len(orders)
            order.vendor.save()
        except Exception as e:
            print (e)
            pass


class JobType(_Model):
    """
        Job type model
    """
    class Meta:
        db_table = "job_types"

    title = CharField()

    def to_dict(self):
        return get_dictionary_from_model(self)

    def __unicode__(self):
        return dict({'id': self.id, 'title': self.title})


class AccountJobType(_Model):
    """
        Model with many-to-many relation:
         jobs that providers able to do
    """
    class Meta:
        db_table = "accounts_job_types"

    account = ForeignKeyField(Account, related_name="job_type_accounts")
    job_type = ForeignKeyField(JobType, related_name="account_job_types")
    hourly_rate = SmallIntegerField(null=True)

    def to_dict(self):
        return dict(self._data.items())


class Gig(_Model):
    """
        Work Model
    """
    class Meta:
        db_table = "gig"

    _type = ForeignKeyField(JobType, related_name="work_type")
    description = TextField(null=True)
    price = IntegerField(null=True)
    account = ForeignKeyField(Account, related_name="work_type_accounts")
    vendor = ForeignKeyField(Account, null=True, related_name="gigs")
    is_active = BooleanField(default=True)

    def get_gig(self):
        gig = get_dictionary_from_model(self)
        gig.update({"account": Account.get(id=gig['account']).get_profile()})
        gig.update({"_type": JobType.get(id=gig['_type']).to_dict()})
        return gig

    def _close_gig(self):
        self.is_active = False
        self.save()


class Order(_Model):
    """
        Order Model
    """
    class Meta:
        db_table = "orders"

    status = SmallIntegerField(null=True, default=1)  # order type: 1 - pending | 2 - accepted | 3 - rejected
    description = TextField(null=True)
    vendor = ForeignKeyField(Account, related_name="orders")
    gig = ForeignKeyField(Gig, related_name='orders')
    created = DateTimeField(default=peewee_now)
    hours_worked = SmallIntegerField(null=True)
    money_to_pay = DecimalField(null=True, decimal_places=2)
    client_feedback = TextField(null=True)
    vendor_feedback = TextField(null=True)
    response_rating = DecimalField(null=True, max_digits=10, decimal_places=2)
    satisfaction_rating = DecimalField(null=True, max_digits=10, decimal_places=2)

    def get_order(self):
        data = {key: item for key, item in self._data.items()}
        data['vendor'] = self.vendor.get_data()
        data['gig'] = self.gig.get_gig()
        data['gig']['account'] = self.gig.account.get_data()
        return data

    def checkstatus_and_save(self):
        if (self.status == OrderStatus.Accepted.value) or (self.status == OrderStatus.Rejected.value):
            self.gig._close_gig()
        self.save()

    def finish_order(self, hours_worked, vendor_feedback, response_rating, satisfaction_rating):
        self.status = OrderStatus.Finished.value
        with db.transaction():
            self.hours_worked = int(hours_worked)
            self.money_to_pay = self.gig.price * self.hours_worked
            self.vendor_feedback = vendor_feedback
            self.response_rating = response_rating
            self.satisfaction_rating = satisfaction_rating
            self.save()
        Account.calculate_rating(self)

    @staticmethod
    def get_orders(account_id, order_type, status_id=None):
        condition = True
        if order_type == 'vendor':
            condition = (Order.vendor == account_id)
        elif order_type == 'customer':
            condition = (Gig.account == account_id)

        # condition = (Order.status != OrderStatus.Finished.value)
        if status_id:
            condition &= (Order.status == status_id)
        orders = Order.select().join(Gig).where(condition).order_by(-Order.created)
        return orders


class Payment(_Model):
    """
        Payment Model
    """
    class Meta:
        db_table = "payments"

    account = ForeignKeyField(Account, related_name='client_payments')
    order = ForeignKeyField(Order, related_name='payments')
    payment_id = CharField(max_length=255)  # e.g "PAY-57363176S1057143SKE2HO3A"
    created = DateTimeField(default=peewee_now)

    def get_payment(self):
        data = {key: item for key, item in self._data.items()}
        data['account'] = self.account.get_data()
        data['order'] = self.order.get_order()
        return data


def fill_db():
    try:
        job_types = ['Websites design', 'Marketing', 'Plumbing', 'Babysitter', 'Grocery Shopping',
                     'Fast Food/conveniences delivery', 'Maid service', 'Painting', 'Yardwork', 'Home repairs',
                     'Personal shopper', 'Pet Grooming', 'Pet sitting', 'Dog Walker', 'Notary Services',
                     'Legal Services', 'Writing', 'Translations', 'Beauty/Salon services', 'Automotive Repair',
                     'Bartending/Foodservice', 'Cooks/chef/baker (cakes)', 'Blogger', 'Outbound Call Agent',
                     'Photographer', 'Music Bands', 'Computer Aid', 'Tailor', 'Accounting/Tax', 'Psychic',
                     'Marriage Officiant', 'Exterminator', 'Pool repairs/maintenance',
                     'Odd Ball Gigs (profiles can add these jobs)',
                     'Cell phone Help', 'Nursing Aids', 'Performers', 'Tutors']
        for jb in job_types:
            with db.transaction():
                JobType.create(title=jb)

        account = Account(email="test@example.com",
                          password=sha1("123").hexdigest(),
                          first_name="test",
                          last_name="last_name",
                          phone="+498963648018",
                          alt_phone="+498963648018",
                          address="New York, Steinway Street, 5",
                          pay_pal="test@paypal.com"
                          )
        account.set_zip_code("04116")
        account.save()

        account = Account(email="test1@example.com",
                          password=sha1("123").hexdigest(),
                          first_name="test1",
                          last_name="last_name1",
                          phone="+498963648018",
                          alt_phone="+498963648018",
                          address="New York, Christopher Street, 9",
                          pay_pal="test1@paypal.com"
                          )
        account.set_zip_code("04120")
        account.save()

        zip_codes = ['01135', '50055', '01032']

        for i in range(2, 4):
            print i
            email = 'test%d@example.com' % i
            password = sha1("123").hexdigest()
            first_name = "test%d" % i
            last_name = "last_name%d" % i

            zip_code = zip_codes[i-1]

            if i % 3:
                vendor_status = '1'
            else:
                vendor_status = '2'
            type = 2

            with db.transaction():
                account = Account(email=email,
                                  password=password,
                                  first_name=first_name,
                                  last_name=last_name,
                                  vendor_status=vendor_status,
                                  type=type)
                account.set_zip_code(zip_code)
                account.save()

        for j in range(1, 5):
            with db.transaction():
                account_job_type = AccountJobType(account=Account.select().where(Account.id == j),
                                                  job_type=JobType.select().where(JobType.id == j),
                                                  hourly_rate=10+j)

                account_job_type.save()
            # with db.transaction():
            #     gig = Gig(_type=j,
            #               description="Gig %i description" % j,
            #               price=12+j,
            #               account=Account.select().where((Account.type == 3) | (Account.type == 1))[0],
            #               vendor=None)
            #     gig.save()

            # vendor = Account.select().where((Account.type == 3) | (Account.type == 2))[0]
            # gigs = Gig.select()
            # for gig in gigs:
            #     status = random.randint(1, 4)
            #     order = Order(vendor=vendor,
            #                   gig=gig,
            #                   status=status,
            #                   response_rating=random.randint(0, 5) if status == 4 else None,
            #                   satisfaction_rating=random.randint(0, 5) if status == 4 else None)
            #     order.save()
    except:
        db.rollback()
        raise



