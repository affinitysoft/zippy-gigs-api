from zippy_gig.base import BaseController, ApiException
from zippy_gig.models import db, Order
from flask import g


class FinishOrderController(BaseController):

    def __init__(self, request):
        super(FinishOrderController, self).__init__(request)

    def _call(self):
        account = g.account
        hours_worked = self._verify_field("hours_worked")
        vendor_feedback = self._verify_field("vendor_feedback")
        order_id = self._verify_field("order")
        response_rating = self._validate_rating(self._verify_field("response_rating"))
        satisfaction_rating = self._validate_rating(self._verify_field("satisfaction_rating"))
        order = self._check_order(order_id=order_id, account=account)
        order.finish_order(self._check_hours_worked(hours_worked), vendor_feedback,
                           response_rating, satisfaction_rating)
        return order.get_order()

    def _check_order(self, account, order_id):
        try:
            order = Order.get(Order.id == order_id)
            if account == order.vendor:
                return order
            else:
                raise ApiException("Order (id=%s) doesn't belong to account" % order_id)
        except Order.DoesNotExist:
            raise ApiException("Order (id=%s) doesn't exist" % order_id)

    @staticmethod
    def _check_hours_worked(hours_worked):
        try:
            hours_worked = int(hours_worked)
        except ValueError:
            raise ApiException("Hours of work must be a number")
        if hours_worked > 0:
            return hours_worked
        else:
            raise ApiException("Hours of work must be greater than 0")

    def _validate_rating(self, rating):
        try:
            rating = int(rating)
        except ValueError:
            raise ApiException("Rating must be a number")

        if rating > 5:
            raise ApiException("Rating must be in range from 0 to 5")
        return rating

