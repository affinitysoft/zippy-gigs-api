from zippy_gig.base import BaseController, ApiException
from zippy_gig.models import db, Gig, Order
from zippy_gig.constants import AccountType, OrderStatus
from flask import g


class ChangeOrderStatusController(BaseController):

    def __init__(self, request):
        super(ChangeOrderStatusController, self).__init__(request)

    def _call(self):
        account = g.account
        status_id = self._verify_field("status_id") # order type: 1 - pending | 2 - accepted | 3 - rejected
        order_id = self._verify_field("order_id")
        return self._change_order_status(account, order_id, status_id).get_order()

    def _change_order_status(self, account, order_id, status_id):
        if account.type in (AccountType.Client.value, AccountType.ClientAndVendor.value):
            with db.transaction():
                order = self._check_order(order_id)
                order.status = self._check_order_status(status_id)
                order.checkstatus_and_save()
            return order
        else:
            raise ApiException("This account is not a client")

    def _check_order(self, order_id):
        try:
            order = Order.get(Order.id == order_id)
            return order
        except Order.DoesNotExist:
            raise ApiException("Order (id=%s) doesn't exist" % order_id)

    def _check_order_status(self, status_id):
        if status_id in (OrderStatus.Accepted.value, OrderStatus.Rejected.value, OrderStatus.Pending.value):
            return status_id
        else:
            raise ApiException("OrderStatus (id=%s) doesn't exist" % status_id)
