from flask import g
from zippy_gig.base import BaseController, ApiException
from zippy_gig.models import Order, Gig
from zippy_gig.constants import PAGINATION_ORDER_PER_PAGE
from math import ceil
from zippy_gig.constants import OrderStatus


class GetOrdersController(BaseController):

    def __init__(self, request):
        super(GetOrdersController, self).__init__(request)

    def _call(self):
        '''
            Check get parameters and get query
        '''
        account = g.account
        page = self._verify_param("page")  # check pagination page in get params
        page = int(page) if page else 1
        status_id = self._verify_param("status")
        order_type = self._verify_param("order_type")
        status_id = int(status_id) if status_id else None
        orders = Order.get_orders(account_id=account.id,
                                  status_id=self._check_order_status(status_id),
                                  order_type=order_type)
        data = {}
        data['pagination'] = self._create_pagination_info(query=orders,
                                                          item_per_page=PAGINATION_ORDER_PER_PAGE,
                                                          page=page)
        orders = orders.paginate(page, PAGINATION_ORDER_PER_PAGE)
        data['orders'] = [_order.get_order() for _order in orders]
        return data

    def _verify_param(self, name):
        return self.request.args.get(name, None)

    def _create_pagination_info(self, query, item_per_page, page):
        data = {}
        data['pages'] = int(ceil(query.count() / float(item_per_page)))
        data['current_page'] = page
        data['prev_page'] = (page-1) if page > 1 else None
        data['next_page'] = (page+1) if page < data['pages'] else None
        return data

    def _check_order_status(self, status_id=None):
        if status_id:
            if status_id in (OrderStatus.Accepted.value, OrderStatus.Rejected.value,
                             OrderStatus.Pending.value, OrderStatus.Finished.value):
                return status_id
            else:
                raise ApiException("OrderStatus (id=%s) doesn't exist" % status_id)
        return None
