from zippy_gig.base import BaseController, ApiException
from zippy_gig.models import Order


class GetOrderController(BaseController):

    def __init__(self, request):
        super(GetOrderController, self).__init__(request)

    def _call(self, id):
        try:
            return Order.get(Order.id == id).get_order()
        except Order.DoesNotExist:
            raise ApiException('Order with id: %s does not exist' % id)
