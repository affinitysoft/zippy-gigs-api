from flask import Blueprint, request
from zippy_gig.decorators import jsonify_result
from zippy_gig import token_auth
from zippy_gig.order.create_order import CreateOrderController
from zippy_gig.order.change_order_status import ChangeOrderStatusController
from zippy_gig.order.get_orders import GetOrdersController
from zippy_gig.order.get_order import GetOrderController
from zippy_gig.order.finish_order import FinishOrderController

order = Blueprint('order', __name__, url_prefix='/api/v1/order')


@order.route("/create-order/", methods=['POST'])
@token_auth.login_required
@jsonify_result
def add_order():
    return CreateOrderController(request)()


@order.route("/change-order-status/", methods=['POST'])
@token_auth.login_required
@jsonify_result
def change_order_status():
    return ChangeOrderStatusController(request)()


@order.route("/get-orders/", methods=['GET'])
@token_auth.login_required
@jsonify_result
def get_orders():
    return GetOrdersController(request)()


@order.route("/get-orders/<int:id>/", methods=['GET'])
@jsonify_result
def get_order(id):
    return GetOrderController(request)(id)


@order.route("/finish-order/", methods=['POST'])
@token_auth.login_required
@jsonify_result
def finish_order():
    return FinishOrderController(request)()

