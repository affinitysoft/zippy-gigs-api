from zippy_gig.base import BaseController, ApiException
from zippy_gig.models import db, Gig, Order
from zippy_gig.constants import AccountType, OrderStatus
from flask import g


class CreateOrderController(BaseController):

    def __init__(self, request):
        super(CreateOrderController, self).__init__(request)

    def _call(self):
        account = g.account
        description = self.request.get_json().get('description', None)
        status_id = self.request.get_json().get('status_id', None)
        gig_id = self._verify_field("gig_id")
        return self._create_order(account, gig_id, description, status_id).get_order()

    def _create_order(self, account, gig_id, description=None, status_id=None):
        if account.type in (AccountType.Vendor.value, AccountType.ClientAndVendor.value):
            with db.transaction():
                order = Order.get_or_create(vendor=account,
                                            gig=self._check_gig(gig_id),
                                            )
                if order[1]:
                    if description:
                        order[0].description = description
                        order[0].save()
                    if status_id:
                        order[0].status = self._check_order_status(status_id)
                        order[0].checkstatus_and_save()
                else:
                    raise ApiException("Order for this Gig (id=%s) and this account already exists" % gig_id)
            return order[0]
        else:
            raise ApiException("This account is not a vendor")

    def _check_gig(self, gig_id):
        try:
            gig = Gig.get(Gig.id == gig_id)
            return gig
        except Gig.DoesNotExist:
            raise ApiException("Gig (id=%s) doesn't exist" % gig_id)

    def _check_order_status(self, status_id):
        if status_id in (OrderStatus.Accepted.value, OrderStatus.Rejected.value, OrderStatus.Pending.value):
            return status_id
        else:
            raise ApiException("OrderStatus (id=%s) doesn't exist" % status_id)
