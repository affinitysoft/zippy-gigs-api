# Zippy Gigs API #

### DOCS ###

* [API Documentation](http://212.24.102.216/) (not finished)

## Running on localhost ##

1) Clone project to your local machine
```
#!bash

git clone https://icegeric@bitbucket.org/affinitysoft/zippy-gigs-api.git
```

2) Go to cloned folder

```
#!bash

cd zippy-gigs-api/
```

3) Create virtual environment for project
```
#!bash

virtualenv venv
```

4) Switch to virtual environment

```
#!bash

source venv/bin/activate
```

5) Install all dependencies listed in requirements.txt to virtual environment

```
#!bash

pip install -r requirements.txt
```
6) Install PostgreSQL (depends on your operation system) and execute next commands: 

```
#!sql

psql
create user zippy_gig with password '123';
create database zippy_db with owner zippy_gig;
```

7) Generate tables and fill initial data

```
#!python

python init_db.py
```

8) Run application on localhost

```
#!python

python runserver.py
```